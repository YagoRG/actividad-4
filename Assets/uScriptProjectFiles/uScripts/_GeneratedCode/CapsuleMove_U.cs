//uScript Generated Code - Build 1.0.3101
//Generated with Debug Info
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[NodePath("Graphs")]
[System.Serializable]
[FriendlyName("CapsuleRotate_U", "")]
public class CapsuleMove_U : uScriptLogic
{

   #pragma warning disable 414
   GameObject parentGameObject = null;
   uScript_GUI thisScriptsOnGuiListener = null; 
   bool m_RegisteredForEvents = false;
   delegate void ContinueExecution();
   ContinueExecution m_ContinueExecution;
   bool m_Breakpoint = false;
   const int MaxRelayCallCount = 1000;
   int relayCallCount = 0;
   
   //externally exposed events
   
   //external parameters
   
   //local nodes
   public UnityEngine.Vector3 direccion = new Vector3( (float)0, (float)0, (float)0 );
   UnityEngine.Vector3 local_10_UnityEngine_Vector3 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single local_24_System_Single = (float) 0;
   System.Single local_6_System_Single = (float) 0;
   public System.Single speed = (float) 0;
   
   //owner nodes
   UnityEngine.GameObject owner_Connection_7 = null;
   
   //logic nodes
   //pointer to script instanced logic node
   uScriptAct_ClampVector3 logic_uScriptAct_ClampVector3_uScriptAct_ClampVector3_1 = new uScriptAct_ClampVector3( );
   UnityEngine.Vector3 logic_uScriptAct_ClampVector3_Target_1 = new Vector3( );
   System.Boolean logic_uScriptAct_ClampVector3_ClampX_1 = (bool) false;
   System.Single logic_uScriptAct_ClampVector3_XMin_1 = (float) 0;
   System.Single logic_uScriptAct_ClampVector3_XMax_1 = (float) 0;
   System.Boolean logic_uScriptAct_ClampVector3_ClampY_1 = (bool) false;
   System.Single logic_uScriptAct_ClampVector3_YMin_1 = (float) 0;
   System.Single logic_uScriptAct_ClampVector3_YMax_1 = (float) 0;
   System.Boolean logic_uScriptAct_ClampVector3_ClampZ_1 = (bool) false;
   System.Single logic_uScriptAct_ClampVector3_ZMin_1 = (float) 0;
   System.Single logic_uScriptAct_ClampVector3_ZMax_1 = (float) 0;
   UnityEngine.Vector3 logic_uScriptAct_ClampVector3_Result_1;
   bool logic_uScriptAct_ClampVector3_Out_1 = true;
   //pointer to script instanced logic node
   uScriptAct_GetDeltaTime logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_4 = new uScriptAct_GetDeltaTime( );
   System.Single logic_uScriptAct_GetDeltaTime_DeltaTime_4;
   System.Single logic_uScriptAct_GetDeltaTime_SmoothDeltaTime_4;
   System.Single logic_uScriptAct_GetDeltaTime_FixedDeltaTime_4;
   bool logic_uScriptAct_GetDeltaTime_Out_4 = true;
   //pointer to script instanced logic node
   uScriptAct_MultiplyFloat_v2 logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_5 = new uScriptAct_MultiplyFloat_v2( );
   System.Single logic_uScriptAct_MultiplyFloat_v2_A_5 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_B_5 = (float) 0;
   System.Single logic_uScriptAct_MultiplyFloat_v2_FloatResult_5;
   System.Int32 logic_uScriptAct_MultiplyFloat_v2_IntResult_5;
   bool logic_uScriptAct_MultiplyFloat_v2_Out_5 = true;
   //pointer to script instanced logic node
   uScriptAct_MultiplyVector3WithFloat logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_9 = new uScriptAct_MultiplyVector3WithFloat( );
   UnityEngine.Vector3 logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_9 = new Vector3( (float)0, (float)0, (float)0 );
   System.Single logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_9 = (float) 0;
   UnityEngine.Vector3 logic_uScriptAct_MultiplyVector3WithFloat_Result_9;
   bool logic_uScriptAct_MultiplyVector3WithFloat_Out_9 = true;
   
   //event nodes
   UnityEngine.GameObject event_UnityEngine_GameObject_Instance_0 = default(UnityEngine.GameObject);
   
   //property nodes
   
   //method nodes
   UnityEngine.Vector3 method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_translation_8 = new Vector3( );
   #pragma warning restore 414
   
   //functions to refresh properties from entities
   
   void SyncUnityHooks( )
   {
      SyncEventListeners( );
      if ( null == owner_Connection_7 || false == m_RegisteredForEvents )
      {
         owner_Connection_7 = parentGameObject;
      }
   }
   
   void RegisterForUnityHooks( )
   {
      SyncEventListeners( );
   }
   
   void SyncEventListeners( )
   {
      if ( null == event_UnityEngine_GameObject_Instance_0 || false == m_RegisteredForEvents )
      {
         event_UnityEngine_GameObject_Instance_0 = uScript_MasterComponent.LatestMaster;
         if ( null != event_UnityEngine_GameObject_Instance_0 )
         {
            {
               uScript_Update component = event_UnityEngine_GameObject_Instance_0.GetComponent<uScript_Update>();
               if ( null == component )
               {
                  component = event_UnityEngine_GameObject_Instance_0.AddComponent<uScript_Update>();
               }
               if ( null != component )
               {
                  component.OnUpdate += Instance_OnUpdate_0;
                  component.OnLateUpdate += Instance_OnLateUpdate_0;
                  component.OnFixedUpdate += Instance_OnFixedUpdate_0;
               }
            }
         }
      }
   }
   
   void UnregisterEventListeners( )
   {
      if ( null != event_UnityEngine_GameObject_Instance_0 )
      {
         {
            uScript_Update component = event_UnityEngine_GameObject_Instance_0.GetComponent<uScript_Update>();
            if ( null != component )
            {
               component.OnUpdate -= Instance_OnUpdate_0;
               component.OnLateUpdate -= Instance_OnLateUpdate_0;
               component.OnFixedUpdate -= Instance_OnFixedUpdate_0;
            }
         }
      }
   }
   
   public override void SetParent(GameObject g)
   {
      parentGameObject = g;
      
      logic_uScriptAct_ClampVector3_uScriptAct_ClampVector3_1.SetParent(g);
      logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_4.SetParent(g);
      logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_5.SetParent(g);
      logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_9.SetParent(g);
      owner_Connection_7 = parentGameObject;
   }
   public void Awake()
   {
      
   }
   
   public void Start()
   {
      SyncUnityHooks( );
      m_RegisteredForEvents = true;
      
   }
   
   public void OnEnable()
   {
      RegisterForUnityHooks( );
      m_RegisteredForEvents = true;
   }
   
   public void OnDisable()
   {
      UnregisterEventListeners( );
      m_RegisteredForEvents = false;
   }
   
   public void Update()
   {
      //reset each Update, and increments each method call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      if ( null != m_ContinueExecution )
      {
         ContinueExecution continueEx = m_ContinueExecution;
         m_ContinueExecution = null;
         m_Breakpoint = false;
         continueEx( );
         return;
      }
      UpdateEditorValues( );
      
      //other scripts might have added GameObjects with event scripts
      //so we need to verify all our event listeners are registered
      SyncEventListeners( );
      
   }
   
   public void OnDestroy()
   {
   }
   
   void Instance_OnUpdate_0(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnUpdate_0( );
   }
   
   void Instance_OnLateUpdate_0(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnLateUpdate_0( );
   }
   
   void Instance_OnFixedUpdate_0(object o, System.EventArgs e)
   {
      //reset event call
      //if it ever goes above MaxRelayCallCount before being reset
      //then we assume it is stuck in an infinite loop
      if ( relayCallCount < MaxRelayCallCount ) relayCallCount = 0;
      
      //fill globals
      //relay event to nodes
      Relay_OnFixedUpdate_0( );
   }
   
   void Relay_OnUpdate_0()
   {
      if (true == CheckDebugBreak("24c40b3b-6ade-49e9-82a8-e2c202584273", "Global_Update", Relay_OnUpdate_0)) return; 
      Relay_In_1();
   }
   
   void Relay_OnLateUpdate_0()
   {
      if (true == CheckDebugBreak("24c40b3b-6ade-49e9-82a8-e2c202584273", "Global_Update", Relay_OnLateUpdate_0)) return; 
   }
   
   void Relay_OnFixedUpdate_0()
   {
      if (true == CheckDebugBreak("24c40b3b-6ade-49e9-82a8-e2c202584273", "Global_Update", Relay_OnFixedUpdate_0)) return; 
   }
   
   void Relay_In_1()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("00d96769-3030-455f-8978-bac7d35262d9", "Clamp_Vector3", Relay_In_1)) return; 
         {
            {
               logic_uScriptAct_ClampVector3_Target_1 = direccion;
               
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_ClampVector3_uScriptAct_ClampVector3_1.In(logic_uScriptAct_ClampVector3_Target_1, logic_uScriptAct_ClampVector3_ClampX_1, logic_uScriptAct_ClampVector3_XMin_1, logic_uScriptAct_ClampVector3_XMax_1, logic_uScriptAct_ClampVector3_ClampY_1, logic_uScriptAct_ClampVector3_YMin_1, logic_uScriptAct_ClampVector3_YMax_1, logic_uScriptAct_ClampVector3_ClampZ_1, logic_uScriptAct_ClampVector3_ZMin_1, logic_uScriptAct_ClampVector3_ZMax_1, out logic_uScriptAct_ClampVector3_Result_1);
         direccion = logic_uScriptAct_ClampVector3_Result_1;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_ClampVector3_uScriptAct_ClampVector3_1.Out;
         
         if ( test_0 == true )
         {
            Relay_In_4();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CapsuleMove_U.uscript at Clamp Vector3.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_4()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("39cf6a05-f2f1-410f-8ede-2536a4fb2044", "Get_Delta_Time", Relay_In_4)) return; 
         {
            {
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_4.In(out logic_uScriptAct_GetDeltaTime_DeltaTime_4, out logic_uScriptAct_GetDeltaTime_SmoothDeltaTime_4, out logic_uScriptAct_GetDeltaTime_FixedDeltaTime_4);
         local_6_System_Single = logic_uScriptAct_GetDeltaTime_DeltaTime_4;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_GetDeltaTime_uScriptAct_GetDeltaTime_4.Out;
         
         if ( test_0 == true )
         {
            Relay_In_5();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CapsuleMove_U.uscript at Get Delta Time.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_5()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("80ec8054-de67-4347-bc44-f58b7a787c9a", "Multiply_Float", Relay_In_5)) return; 
         {
            {
               logic_uScriptAct_MultiplyFloat_v2_A_5 = speed;
               
            }
            {
               logic_uScriptAct_MultiplyFloat_v2_B_5 = local_6_System_Single;
               
            }
            {
            }
            {
            }
         }
         logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_5.In(logic_uScriptAct_MultiplyFloat_v2_A_5, logic_uScriptAct_MultiplyFloat_v2_B_5, out logic_uScriptAct_MultiplyFloat_v2_FloatResult_5, out logic_uScriptAct_MultiplyFloat_v2_IntResult_5);
         local_24_System_Single = logic_uScriptAct_MultiplyFloat_v2_FloatResult_5;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_MultiplyFloat_v2_uScriptAct_MultiplyFloat_v2_5.Out;
         
         if ( test_0 == true )
         {
            Relay_In_9();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CapsuleMove_U.uscript at Multiply Float.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_Translate_8()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("284ea245-9e36-42bc-9463-088a7a2b82bb", "UnityEngine_Transform", Relay_Translate_8)) return; 
         {
            {
               method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_translation_8 = local_10_UnityEngine_Vector3;
               
            }
         }
         {
            UnityEngine.Transform component;
            component = owner_Connection_7.GetComponent<UnityEngine.Transform>();
            if ( null != component )
            {
               component.Translate(method_Detox_ScriptEditor_Plug_UnityEngine_GameObject_translation_8);
            }
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CapsuleMove_U.uscript at UnityEngine.Transform.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   void Relay_In_9()
   {
      if ( relayCallCount++ < MaxRelayCallCount )
      {
         if (true == CheckDebugBreak("9261138e-fc1b-434d-ba64-331f3568eb43", "Multiply_Vector3_With_Float", Relay_In_9)) return; 
         {
            {
            }
            {
               logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_9 = local_24_System_Single;
               
            }
            {
            }
         }
         logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_9.In(logic_uScriptAct_MultiplyVector3WithFloat_targetVector3_9, logic_uScriptAct_MultiplyVector3WithFloat_targetFloat_9, out logic_uScriptAct_MultiplyVector3WithFloat_Result_9);
         local_10_UnityEngine_Vector3 = logic_uScriptAct_MultiplyVector3WithFloat_Result_9;
         
         //save off values because, if there are multiple, our relay logic could cause them to change before the next value is tested
         bool test_0 = logic_uScriptAct_MultiplyVector3WithFloat_uScriptAct_MultiplyVector3WithFloat_9.Out;
         
         if ( test_0 == true )
         {
            Relay_Translate_8();
         }
      }
      else
      {
         uScriptDebug.Log( "Possible infinite loop detected in uScript CapsuleMove_U.uscript at Multiply Vector3 With Float.  If this is in error you can change the Maximum Node Recursion in the Preferences Panel and regenerate the script.", uScriptDebug.Type.Error);
      }
   }
   
   private void UpdateEditorValues( )
   {
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CapsuleMove_U.uscript:speed", speed);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "90e0e81f-6105-44ad-8781-b26c40e96fae", speed);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CapsuleMove_U.uscript:direccion", direccion);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "b5f6bfd0-c8bd-412d-94f9-0f5f78e2f554", direccion);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CapsuleMove_U.uscript:6", local_6_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "5f04a8e6-6655-4f9d-9de1-3b2d1f4c047c", local_6_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CapsuleMove_U.uscript:10", local_10_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "6d81fa95-c43f-4b24-821c-6909518b2a93", local_10_UnityEngine_Vector3);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "CapsuleMove_U.uscript:24", local_24_System_Single);
      uScript_MasterComponent.LatestMasterComponent.UpdateNodeValue( "63391cb4-9413-47b0-86b5-8332ea383bf3", local_24_System_Single);
   }
   bool CheckDebugBreak(string guid, string name, ContinueExecution method)
   {
      if (true == m_Breakpoint) return true;
      
      if (true == uScript_MasterComponent.FindBreakpoint(guid))
      {
         if (uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint == guid)
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = "";
         }
         else
         {
            uScript_MasterComponent.LatestMasterComponent.CurrentBreakpoint = guid;
            UpdateEditorValues( );
            UnityEngine.Debug.Log("uScript BREAK Node:" + name + " ((Time: " + Time.time + "");
            UnityEngine.Debug.Break();
            #if (!UNITY_FLASH)
            m_ContinueExecution = new ContinueExecution(method);
            #endif
            m_Breakpoint = true;
            return true;
         }
      }
      return false;
   }
}
