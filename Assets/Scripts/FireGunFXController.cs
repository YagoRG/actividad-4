﻿using UnityEngine;

public class FireGunFXController : MonoBehaviour {

    public ParticleSystem weaponFireParticles;
    public Light weaponFireLight;
    public Transform fxOrigin;


    public void Play() {
        fxOrigin.transform.localRotation = Quaternion.Euler(0, 0, Random.Range(0f, 360f));
        weaponFireParticles.Play(true);
    }


    // Update is called once per frame
    void Update() {
        fxOrigin.transform.localPosition = fxOrigin.localPosition;
        weaponFireLight.enabled = weaponFireParticles.isPlaying;
    }
}
