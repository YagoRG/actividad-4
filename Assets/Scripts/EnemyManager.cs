﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public CharacterHealth playerHealth;       // Referencia a la vida del jugador.
    public GameObject enemy;                // Prefab enemigo que se va a generar.
    public float spawnTime = 3f;            // Tiempo entre spawns.
    public Transform[] spawnPoints;         // Lugares desde os que va a Spawnear.


    void Start()
    {
        // Llamar a la función de spawn despues de un periodo de tiempo establecido por el usuario y luego seguir generando esperando el mismo tiempo.
        InvokeRepeating("Spawn", spawnTime, spawnTime);
    }


    void Spawn()
    {
        // Si el jugador está muerto...
        if (playerHealth.currentHealth <= 0f)
        {
            // ... salir de la función.
            return;
        }

        // Seleccionar aleatoriamente un pundo de Spawn si hay varios posibles para un mismo spawner.
        int spawnPointIndex = Random.Range(0, spawnPoints.Length);

        // Crear un enemigo a parte del prefab en una posición aleatoria dentro de las estipuladas con una rotción aleatoria.
        Instantiate(enemy, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
    }
}