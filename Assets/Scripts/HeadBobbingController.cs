﻿using System;
using UnityEngine;

public class HeadBobbingController : MonoBehaviour {

    public HeadBobbingSystem.HeadBobber gunCameraHB;
    public HeadBobbingState[] headBobberConfigs = null;     // Force the array to be null before the first execution of OnValidate event

    private void OnValidate() {

        // First time OnValidate is executed, the headBobberConfigs array must be null
        if (headBobberConfigs == null) {

            // Instantiate the array with the same size as the PLAYER_STATES enum number of elements
            headBobberConfigs = new HeadBobbingState[Enum.GetValues(typeof(PlayerFPSController.PLAYER_STATES)).Length];

            // Create a fresh HeadBobbing State in each of the array positions
            for (int i = 0; i < headBobberConfigs.Length; i++) {
                headBobberConfigs[i] = new HeadBobbingState();
            }
        }

        // If the designer modifies the size of the array that contains the differents configurations...
        if (headBobberConfigs.Length != Enum.GetValues(typeof(PlayerFPSController.PLAYER_STATES)).Length) {
            Debug.LogWarning("Don't change the 'headBobberConfigs' field's array size or you lost the config data!");

            //... resize the array to its correct size
            Array.Resize(ref headBobberConfigs, Enum.GetValues(typeof(PlayerFPSController.PLAYER_STATES)).Length);

            // If the new size value introduced by the designer is less than the original size, it will lead to a lost of data
            // so those configs that may had been missed need to be created again
            for (int i = 0; i < headBobberConfigs.Length; i++) {
                if(headBobberConfigs[i] == null)
                    headBobberConfigs[i] = new HeadBobbingState();
             }
        }

        // Set the state and name of each array element to its equivalent in the enum
        for (int i = 0; i < headBobberConfigs.Length; i++) {
            headBobberConfigs[i].stateHBConfig = (PlayerFPSController.PLAYER_STATES)i;
            headBobberConfigs[i].stateName = headBobberConfigs[i].stateHBConfig.ToString();
        }

    }


    public void setBobbingConfig(PlayerFPSController.PLAYER_STATES playerCurrentState, float bobbingAmount) {
        gunCameraHB.setConfig(headBobberConfigs[(int)playerCurrentState].hbData);
        gunCameraHB.setBobbing(bobbingAmount);
    }


    [Serializable]
    public class HeadBobbingState {
        [HideInInspector]
        public string stateName;
        [HideInInspector]
        public PlayerFPSController.PLAYER_STATES stateHBConfig;
        public HeadBobbingSystem.HeadBobberData hbData;
    }
}
